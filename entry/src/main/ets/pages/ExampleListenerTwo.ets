/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { Annotations, References } from '@ohos/mbassador'
import { ExampleFilterTwo } from './ExampleFilterTwo'
import { CallBackListener } from './CallBackListener'
import { WidgetsInfo } from './WidgetsInfo'


@Annotations.Listener("ExampleListenerTwo",References.Weak)
export class ExampleListenerTwo {
  call: CallBackListener;

  constructor(call: CallBackListener) {
    this.call = call;
  }

  @Annotations.handle()
  firstMessageTwo(str: WidgetsInfo, listener: ExampleListenerTwo) {
    console.log("asa_ ExampleListenerTwo firstMessage ----:" + str.strName);
    listener.call.callData("ExampleListenerTwo", "firstMessage", str, 0);
  }

  @Annotations.handle({ priority: 100000, enabled: false })
  secondMessageTwo(str: WidgetsInfo, listener: ExampleListenerTwo) {
    console.log("asa_ ExampleListenerTwo secondMessage ----:" + str.strName);
    listener.call.callData("ExampleListenerTwo", "secondMessage", str, 1);
  }

  @Annotations.handle({ filters: [new ExampleFilterTwo()], enabled: true })
  thirdMessageTwo(str: WidgetsInfo, listener: ExampleListenerTwo) {
    console.log("asa_ ExampleListenerTwo thirdMessage ----:" + str.strName);
    listener.call.callData("ExampleListenerTwo", "thirdMessage", str, 2);
  }
}
