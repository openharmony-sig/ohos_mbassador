/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { IMessageDispatcher } from './IMessageDispatcher'
import { SubscriptionContext } from '../subscription/SubscriptionContext'
import type { IHandlerInvocation } from './IHandlerInvocation'

export class MessageDispatcher implements IMessageDispatcher {
    private context: SubscriptionContext;
    private invocation: IHandlerInvocation;

    constructor(context: SubscriptionContext, invocation: IHandlerInvocation) {
        this.context = context;
        this.invocation = invocation;
    }

    dispatch(msg: Object, methodName: String, listener: Object) {
        this.getInvocation().invoke(msg, methodName, listener);
    }

    getContext() {
        return this.context;
    }

    getInvocation(): IHandlerInvocation {
        return this.invocation;
    }
}