/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { IMessagePublication } from '../IMessagePublication'

export class PublicationError {
    private cause: Error;
    private errorStr: string = "";
    private publication: IMessagePublication;

    setCause(e: Error) {
        this.cause = e;
        return this;
    }

    setErrorMessage(str: string) {
        this.errorStr = str;
        return this;
    }

    getCause(): Error {
        return this.cause;
    }

    getErrorMessage(): string {
        return this.errorStr;
    }

    setPublication(p: IMessagePublication) {
        this.publication = p;
        return this;
    }

    getPublication() {
        return this.publication;
    }
}